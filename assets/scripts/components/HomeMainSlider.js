var HomeMainSlider = (function ($) {
    var $featuredSlider;

    
    function start() {
        $('body').removeClass('loading');
    }

    function beforeSlide(slider) {

    }

    function init() {

        $featuredSlider = $('.flexslider--home-featured').flexslider({
            animation: "slide",
            animationSpeed: 650,
            slideshowSpeed: 5000,
            slideshow: true,
            directionNav: false,
            controlNav: true,
            move: 1,
            // manualControls: ".flex-control-nav li",
            // customDirectionNav: $(".featuredSlider .custom-navigation a"),
            mousewheel: false,
            animationLoop: true,
            start: start,
            before : beforeSlide,
            after: function (slider) {
                // Nothing Right Now
            },
            end: function (slider) {
                // Nothing Right Now
            }
        });
    }

    


    return {
        init: init
    };
})(jQuery);