var LoadInstagramImages = (function ($) {

	var clientid = 'f0b48c73b3c3459eb5cef18fb41933f0', // learn how to obtain it below
	    userid = 5219525, // User ID - get it in source HTML of your Instagram profile, use special services or look the next example :)
	    num_photos = 20; // how much photos do you want to get
    var $instagramList = $('.instagram-list');

    function init() {

        
 
		$.ajax({
			url: 'https://api.instagram.com/v1/users/' + userid + '/media/recent?access_token=5219525.f0b48c7.17327f93adbe4fbb89ccd13a1a226a33',
			dataType: 'jsonp',
			type: 'GET',
			data: {client_id: clientid, count: num_photos},
			success: function(data){
		 		//console.log(data);
				for( x in data.data ){
					$instagramList.append('<li class="instagram-list__item"><a href="'+data.data[x].link+'" class="instagram-list__likes" target="_blank"><span class="wrap"><i class="fa fa-heart" aria-hidden="true"></i><span class="count">'+data.data[x].likes.count+'</span></span></a><img src="'+data.data[x].images.standard_resolution.url+'"></li>'); // data.data[x].images.low_resolution.url - URL of image, 306х306
					// data.data[x].images.thumbnail.url - URL of image 150х150
					// data.data[x].images.standard_resolution.url - URL of image 612х612
					// data.data[x].link - Instagram post URL 
				}
			},
			error: function(data){
				console.log(data); // send the error notifications to console
			}
		});

		
    }

    


    return {
        init: init
    };
})(jQuery);