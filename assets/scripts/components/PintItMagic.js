var PintItMagic = (function ($) {

    function init() {

        $('.video-section').off().on({
			    mouseenter: function (e) {
			        //stuff to do on mouse enter
			        e.stopPropagation();
			       // console.log($(this).parent('.pin-magic').length);

			        
			        if($(this).parent('.pin-magic').length === 0){
			        	//console.log('test');
			        	$(this).wrap('<span class="pin-magic"></span>');
			        	$(this).parent('.pin-magic').append('<a class="pin-magic__link" target="_blank"></a>');

			        	var imgurl = $(this).attr('src');
			        	var encodedurl = encodeURIComponent(imgurl);
						var pathname = $(location).attr('href');
						url = encodeURIComponent(pathname);
						var desc = encodeURIComponent('Something Navy');
						var pinhref = 'http://pinterest.com/pin/create/button/?url=';
						pinhref += url;
						pinhref += '&media=';
						pinhref += encodedurl;
						pinhref += '&description=';
						pinhref += desc;
						$(this).parent().find('.pin-magic__link').attr('href',pinhref);
				

			        }
			        
			    },
			    mouseleave: function (e) {
			        //stuff to do on mouse leave
			        e.stopPropagation();
			       
			       // console.log(off);
			    }
			}, ".video__still");

		$('#article-container').off().on({
			    mouseenter: function (e) {
			        //stuff to do on mouse enter
			        e.stopPropagation();
			       // console.log($(this).parent('.pin-magic').length);

			        
			        if($(this).parent('.pin-magic').length === 0){
			        	//console.log('test');
			        	$(this).wrap('<span class="pin-magic"></span>');
			        	$(this).parent('.pin-magic').append('<a class="pin-magic__link" target="_blank"></a>');

			        	var imgurl = $(this).attr('src');
			        	var encodedurl = encodeURIComponent(imgurl);
						var pathname = $(location).attr('href');
						url = encodeURIComponent(pathname);
						var desc = encodeURIComponent('Something Navy');
						var pinhref = 'http://pinterest.com/pin/create/button/?url=';
						pinhref += url;
						pinhref += '&media=';
						pinhref += encodedurl;
						pinhref += '&description=';
						pinhref += desc;
						$(this).parent().find('.pin-magic__link').attr('href',pinhref);
				

			        }
			        
			    },
			    mouseleave: function (e) {
			        //stuff to do on mouse leave
			        e.stopPropagation();
			       
			       // console.log(off);
			    }
			}, ".preview-image--post img");

		$('.main').off().on({
			    mouseenter: function (e) {
			        //stuff to do on mouse enter
			        e.stopPropagation();
			       // console.log($(this).parent('.pin-magic').length);

			        
			        if($(this).find('.pin-magic__link').length === 0){
			        	//console.log('test');
			        	//$(this).wrap('<span class="pin-magic"></span>');
			        	$(this).append('<a class="pin-magic__link" target="_blank"></a>');

			        	var imgurl = $(this).data('bgimage');
			        	var encodedurl = encodeURIComponent(imgurl);
						var pathname = $(location).attr('href');
						url = encodeURIComponent(pathname);
						var desc = encodeURIComponent('Something Navy');
						var pinhref = 'http://pinterest.com/pin/create/button/?url=';
						pinhref += url;
						pinhref += '&media=';
						pinhref += encodedurl;
						pinhref += '&description=';
						pinhref += desc;
						$(this).parent().find('.pin-magic__link').attr('href',pinhref);
				

			        }
			        
			    },
			    mouseleave: function (e) {
			        //stuff to do on mouse leave
			        e.stopPropagation();
			       
			       // console.log(off);
			    }
			}, ".preview-image__placeholder");


		$('#article-container').on('click','.pin-magic__link',function(event){
			//console.log(this);
			var url = $(this).attr('href'),
				windowName = Math.random();
			window.open(url,windowName,"height=450,width=450");
			event.preventDefault();
		});

    }



    return {
        init: init
    };
})(jQuery);