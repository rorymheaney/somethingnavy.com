var FixedNav = (function ($) {

	

    var lastScrollTop = 0,
    	$siteHeader = $('#site-header'),
    	modifedClass = 'site-header-bar--short';

    function init() {


        function stickyScroll(e) {

            
            if ($(window).scrollTop() > 50) {
                $siteHeader.addClass(modifedClass);
            }

            if ($(window).scrollTop() < 50) {
                $siteHeader.removeClass(modifedClass);
            }
        }

        // Scroll handler to toggle classes.
        //window.addEventListener('scroll', stickyScroll, false);
        $(window).on('scroll', function () {
            stickyScroll();
        });

    }


    


    return {
        init: init
    };
    

})(jQuery);
