var RecentlyPinned = (function ($) {

    function init() {

        
 
        $.ajax({
            url: 'https://api.pinterest.com/v3/pidgets/users/ariellecharnas/pins/',
            dataType: 'jsonp',
            type: 'GET',
            success: function(data){
                //console.log(data.data);

          

                if (data.length !== 0) {

                	var pinContent = "";
                	for (var i = 0; i < 1; i++) {

                		var pinImage = data.data.pins[i].images['237x'].url,
                			originalPinImage =  pinImage.replace('237x','originals'),
                			pinLinkID = data.data.pins[i].id;

                		//console.log(originalPinImage);
                		pinContent += '<div class="medium-12 clearfix">'+
            							'<a href="https://www.pinterest.com/pin/'+pinLinkID+'/" class="recent-pins_href opacity-href">'+
            								'<img src="'+originalPinImage+'" alt="recent pin">'+
            							'</a>'+
                					'</div>';
                	}

                	$('#load-pinterest').append(pinContent);
                }
            },
            error: function(data){
                console.log(data); // send the error notifications to console
            }
        });

        
    }

    


    return {
        init: init
    };
})(jQuery);