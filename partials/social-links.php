<li>
  <a href="<?php the_field('facebook_global','option');?>">
    <i class="fa fa-facebook" aria-hidden="true"></i>
  </a>
</li>
<li>
  <a href="<?php the_field('twitter_global','option');?>">
    <i class="fa fa-twitter" aria-hidden="true"></i>
  </a>
</li>
<li>
  <a href="<?php the_field('pinterest_global','option');?>">
    <i class="fa fa-pinterest-p" aria-hidden="true"></i>
  </a>
</li>
<li>
  <a href="<?php the_field('instagram_global','option');?>">
    <i class="fa fa-instagram" aria-hidden="true"></i>
  </a>
</li>
<li>
  <a href="<?php the_field('rss_global','option');?>">
    <i class="fa fa-rss" aria-hidden="true"></i>
  </a>
</li>
<li>
  <a data-dropdown="drop2" aria-controls="drop2" aria-expanded="false">
    <i class="fa fa-snapchat-ghost" aria-hidden="true"></i>
  </a>
  <div id="drop2" data-dropdown-content class="f-dropdown content" aria-hidden="true" tabindex="-1">
    <img src="<?php echo wp_get_attachment_url( '12357' ); ?>" alt="<?php bloginfo('name'); ?>" alt="snapchat"/>
    <a href="https://www.snapchat.com/add/ariellecharnas" style="display: block; margin-top: 10px;">
      @ariellecharnas
    </a>
  </div>
</li>