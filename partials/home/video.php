<?php 
	$getVideoStill = get_field('video_still','option');
?>
<div class="video-section">
	<img class="video-section__still video__still" src="<?php echo $getVideoStill['url'];?>" alt="video still"/>
</div>