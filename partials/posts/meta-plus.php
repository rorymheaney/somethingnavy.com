<div class="content-preview__meta-plus clearfix">
	<!-- comment count -->
	<div class="content-preview__comments">
		<span class="font__details">
			<?php comments_number( 'no Comments', 'one response', '% Comments' ); ?>
		</span>

		
	</div>

	<!-- share -->
	<div class="content-preview__share">
		<span class="font__details">
			<?php echo esc_html( 'Share' ); ?>
		</span>
		<?php echo do_shortcode("[ssbp]"); ?>
	</div>

	<!-- tags -->
	<div class="content-preview__tags">
		<?php
		if(get_the_tag_list()) {
			echo '<span class="font__details">Tags:</span>';
		}
		if(get_the_tag_list()) {
			
		    echo get_the_tag_list('<ul class="tags-list tags-list--content-preview"><li>','</li><li>','</li></ul>');
		}
		?>
	</div>	
</div>