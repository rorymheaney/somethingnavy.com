<div id="contactModal" class="reveal-modal" data-reveal aria-labelledby="modalTitle" aria-hidden="true" role="dialog">
	<h2 class="font__mini-header font__mini-header--other">
		<?php the_field('title_contact_modal','option');?>
	</h2>
	<p class="font__details font__details--bold">
		<?php the_field('sub_title_contact_modal','option');?>
	</p>
	<p class="short-block">
		<em>
			<?php the_field('intro_contact_modal','option');?>
		</em>
	</p>

	<div class="reveal-modal__emails">
		<?php the_field('emails_contact_modal','option');?>
	</div>
	<p class="font__details font__details--bold title-ish">
		<?php the_field('tag_line_contact_modal','option');?>
	</p>
	<?php echo gravity_form(1, false, false, false, '', true, 12);?>
	<ul class="site-header-bar__social-media">
        <?php get_template_part('partials/social-links'); ?>
    </ul>
	<a class="close-reveal-modal" aria-label="Close">&#215;</a>
</div>


<div id="newsletterModal" class="reveal-modal small" data-reveal aria-labelledby="modalTitle" aria-hidden="true" role="dialog">
	<h2 class="font__mini-header font__mini-header--other">
		STAY UPDATED ON ALL THINGS SOMETHING NAVY
	</h2>

	<p class="short-block">
		<em>
			And get exclusive discounts from some of my favorite products through our newsletter!
		</em>
	</p>

	<?php echo gravity_form(3, false, false, false, '', true, 12);?>

	<a class="close-reveal-modal" aria-label="Close">&#215;</a>
</div>


<script data-blogname="SomethingNavy" id="cliqueCX" type="text/javascript" src="http://www.whowhatwear.com/clique-exchange/script.min.js"></script>

<div class="instagram-container">


	<a href="https://www.instagram.com/somethingnavy/" class="instagram-tag" target="_blank">
	  <?php echo esc_html( '@somethingnavy' ); ?>
	</a>

	<ul class="instagram-list clearfix">
	</ul>

</div>
<footer class="content-info footer-content">
	<div class="instagram-feed">
	</div>

	<div class="footer-content__block clearfix">
		<div class="row row--plus">
			<!-- year copyright -->
			<div class="footer-content__space footer-content__year">
				<span>
					<?php echo esc_html( '©' ); ?> <?php echo date('Y'); ?> <?php bloginfo( 'name' ); ?>
				</span>
			</div>

			<!-- links -->
			<div class="footer-content__space footer-content__links footer-content__about">
				<a href="/about/">
					About
				</a>
			</div>

			<!-- links -->
			<div class="footer-content__space footer-content__links">
				<a href="#" data-reveal-id="contactModal">
					Contact
				</a>
			</div>

			<!-- site by -->
			<div class="footer-content__space footer-content__site-by">
				<a href="http://projectmplus.com/" target="_blank">
					<?php echo esc_html( 'Design + development by m plus' ); ?>
				</a>
			</div>

			<?php echo gravity_form(2, true, false, false, '', true, 12);?>
		</div>
	</div>
</footer>
