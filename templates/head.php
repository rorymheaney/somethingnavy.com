<!-- START MOBILE CHECK -->
<script type='text/javascript'>
var isMobile = {
    Android: function () {
        return navigator.userAgent.match(/Android/i) ? true : false;
    },
    BlackBerry: function () {
        return navigator.userAgent.match(/BlackBerry/i) ? true : false;
    },
    iOS: function () {
        return navigator.userAgent.match(/iPhone|iPad|iPod/i) ? true : false;
    },
    Windows: function () {
        return navigator.userAgent.match(/IEMobile/i) ? true : false;
    },
    any: function () {
        return (isMobile.Android() || isMobile.BlackBerry() || isMobile.iOS() || isMobile.Windows());
    }
};
</script>
<!-- END MOBILE CHECK -->

<!-- START GOOGLE DFP HEAD SECTION -->
<script type='text/javascript'>
  //<![CDATA[
    var googletag = googletag || {};
    googletag.cmd = googletag.cmd || [];
    (function() {
        var gads = document.createElement('script');
        gads.async = true;
        gads.type = 'text/javascript';
        var useSSL = 'https:' == document.location.protocol;
        gads.src = (useSSL ? 'https:' : 'http:') +
                '//www.googletagservices.com/tag/js/gpt.js';
        var node = document.getElementsByTagName('script')[0];
        node.parentNode.insertBefore(gads, node);
    })();
  //]]>
</script>
<script type='text/javascript'>
  //<![CDATA[

// DEFINE BLOG ID AND SECTION
var blogString = "somethingnavy";
var currentLocation = (window.location.pathname == '/') ? 'homepage' : 'ros';
var windowPlatform = ( (window.innerWidth >= 768) && (!isMobile.any()) ) ? 'desktop' : 'mobile';

if (windowPlatform == "mobile") {
    //DEFINE MOBILE TAGS
    googletag.cmd.push(function() {
        googletag.defineSlot('/36299600/inf/' + blogString + '/' + currentLocation + '/mobilefooter', [320, 50], 'mobilefooter').addService(googletag.pubads());
        googletag.defineSlot('/36299600/inf/' + blogString + '/' + currentLocation + '/mobilerectangle', [[300, 250], [320, 480]], 'mobilerectangle').addService(googletag.pubads());
        googletag.pubads().enableSingleRequest();
        googletag.pubads().collapseEmptyDivs();
        googletag.enableServices();
    });
} else {
    //DEFINE DESKTOP TAGS
    googletag.cmd.push(function() {
        googletag.defineSlot('/36299600/inf/' + blogString + '/' + currentLocation + '/header', [728, 90], 'cliqueheader').addService(googletag.pubads());
        googletag.defineSlot('/36299600/inf/' + blogString + '/' + currentLocation + '/300multitop', [[300, 600], [300, 250]], '300multitop').addService(googletag.pubads());
        googletag.defineSlot('/36299600/inf/' + blogString + '/' + currentLocation + '/300multibottom', [[300, 600], [300, 250]], '300multibottom').addService(googletag.pubads());
        googletag.pubads().enableSingleRequest();
        googletag.pubads().collapseEmptyDivs();
        googletag.enableServices();
    });
}

  //]]>
</script>
<!-- END GOOGLE DFP HEAD SECTION -->

<head>
  <meta charset="utf-8">
  <meta http-equiv="x-ua-compatible" content="ie=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="icon" type="image/png" href="<?= get_template_directory_uri(); ?>/dist/images/favicon.png">
  <?php wp_head(); ?>


</head>
