<?php
if (post_password_required()) {
  return;
}
?>

<section id="comments" class="comments <?php if(is_page(6861)):?>comments--about<?php else:?>comments--posts<?php endif;?>">
   <?php if(!is_page(6861)):?>
    <p class="comments__intro-title font__details font__details--bold">
    <?php echo esc_html( 'Post A Comment' ); ?>
  </a>
  <?php endif;?>
  

  <?php if(is_page(6861)):?>
      <?php $comment_args = array( 

        'title_reply'=>'',
        'fields' => apply_filters( 'comment_form_default_fields', array(

'author' => '<p class="comment-form-author">' . '<label for="author">' . __( 'Your Good Name' ) . '</label> ' . ( $req ? '<span>*</span>' : '' ) .

        '<input id="author" name="author" type="text" value="default name" size="30"' . $aria_req . ' /></p>',   

    'email'  => '<p class="comment-form-email">' .

                '<label for="email">' . __( 'Your Email Please' ) . '</label> ' .

                ( $req ? '<span>*</span>' : '' ) .

                '<input id="email" name="email" type="text" value="arielle-user@somethingnavy.com" size="30"' . $aria_req . ' />'.'</p>',

    'url'    => '' ) ),
            'comment_field' => '<p>' .

                        '<label for="comment">' . __( '' ) . '</label>' .

                        '<textarea id="comment" name="comment" cols="45" rows="8" aria-required="true" placeholder="Ask Arielle"></textarea>' .

                        '</p>',

            'comment_notes_after' => '',
            'label_submit' => 'submit'

        );

        comment_form($comment_args); ?>
  <?php endif;?>

  <?php if (have_comments()) : ?>

    <ol class="comment-list">
      <?php wp_list_comments(['style' => 'ol', 'short_ping' => true]); ?>
    </ol>

    <?php if (get_comment_pages_count() > 1 && get_option('page_comments')) : ?>
      <nav>
        <ul class="pager">
          <?php if (get_previous_comments_link()) : ?>
            <li class="previous"><?php previous_comments_link(__('&larr; Older comments', 'sage')); ?></li>
          <?php endif; ?>
          <?php if (get_next_comments_link()) : ?>
            <li class="next"><?php next_comments_link(__('Newer comments &rarr;', 'sage')); ?></li>
          <?php endif; ?>
        </ul>
      </nav>
    <?php endif; ?>
  <?php endif; // have_comments() ?>

  <?php if (!comments_open() && get_comments_number() != '0' && post_type_supports(get_post_type(), 'comments')) : ?>
    <div class="alert alert-warning">
      <?php _e('Comments are closed.', 'sage'); ?>
    </div>
  <?php endif; ?>

  <?php if(!is_page(6861)):?>
    <?php comment_form(); ?>
  <?php endif;?>
  
</section>




