<article <?php post_class('content-preview'); ?>>

	<div class="preview-image preview-image--post">
		<?php 
			//is there a singular crop?
			if(get_field('are_you_using_a_single_image_or_two') == "One") :
		?>
			<?php $singleCrop = get_field('singular_image_post');?>
			<div class="preview-image__single-img">
				<img src="<?php echo $singleCrop['url'];?>" alt="image">
			</div>
		<?php 
			//is there a double crop?
			elseif(get_field('are_you_using_a_single_image_or_two') == "Two"):
		?>
			<?php 
				$doubleCropOne = get_field('double_image_one');
				$doubleCropTwo= get_field('double_image_two');
			?>
			<div class="preview-image__double-container">
				<div class="small-6">
					<img src="<?php echo $doubleCropOne['url'];?>" alt="image">
				</div>
				<div class="small-6">
					<img src="<?php echo $doubleCropTwo['url'];?>" alt="image">
				</div>
			</div>
		<?php 
			//featured image?
			elseif (has_post_thumbnail( $post->ID )):
		?>
			<?php 
				$image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'single-post-thumbnail' );
				$image = $image[0];
			?>
			<div class="preview-image__placeholder" data-bgimage="<?php echo $image;?>" style="background-image: url(<?php echo $image;?>);"></div>
		<?php 
			//nothing left to get
			else:
		?>
			<div class="preview-image__placeholder" data-bgimage="<?php echo catch_that_image();?>" style="background-image: url(<?php echo catch_that_image();?>);"></div>
		<?php endif;?>

		
	</div>


	<div class="content-preview__entry-summary">
		<div class="content-preview__summary-con">
			<div class="content-preview__meta">
				<ul class="categories-list categories-list--content-preview">
					<?php 
					  $sep = '';
					  
					  foreach((get_the_category()) as $cat) {
					      echo $sep . '<li class="categories-list__category"><a class="categories-list__href font__sub-head" href="' . get_category_link($cat->term_id) . '"  title="View all posts in '. esc_attr($cat->name) . '">' . $cat->cat_name . '</a></li>';
					  // $sep = ', ';
					      $sep = '';
					  }
					?>
				</ul>

				<?php get_template_part('templates/entry-meta'); ?>
			</div>

			<h2 class="content-preview__entry-title">
				<a class="font__header" href="<?php the_permalink(); ?>">
					
					<?php if(get_field('title_unique')):?>
						<?php the_field('title_unique', false, false);?>
					<?php else:?>
						<?php the_title(); ?>
					<?php endif;?>
				</a>
			</h2>


			<!-- flexslider -->
			<?php get_template_part('partials/posts/flexslider-shopping');?>
			
		</div>
		<!-- summary con END -->
		
		<a class="font__view-more" href="<?php the_permalink(); ?>">
			View More
		</a>
		<?php //the_excerpt(); ?>

		


	</div>

	<!-- meta plus -->
	<?php get_template_part('partials/posts/meta-plus');?>
	<!-- meta plus end -->

</article>
