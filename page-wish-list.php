<div class="button-group filters-button-group">
  <button class="button is-checked" data-filter="*">EVERYTHING</button>
  <button class="button" data-filter=".shopstyle-item--apparel">apparel</button>
  <button class="button" data-filter=".shopstyle-item--beauty">beauty</button>
  <button class="button" data-filter=".shopstyle-item--accessories">accessories</button>
  <button class="button" data-filter=".shopstyle-item--child">children</button>
</div>




<div  class="shopstyle-con">
	<div class="shopstyle-con-list">

		<!-- apparel -->
		<?php

			
			if(($shopStyleApparel = get_transient("shop_style_apparal")) === false) 
			{
				$jsonApparel = file_get_contents('http://api.shopstyle.com/api/v2/lists/47077387/items?pid=uid6041-27578179-93&limit=20');
				$shopStyleApparel = json_decode( $jsonApparel , true );
				set_transient("shop_style_apparal", $shopStyleApparel, 3600);
			} 
			
		// If we have shopStyleApparel
		if ( false !== $shopStyleApparel ) : ?>

		   	<?php// print_r($shopStyleApparel['favorites']);?>


		   <?php foreach( $shopStyleApparel['favorites'] as $image ):?>

		   	<div class="shopstyle-item shopstyle-item--apparel">
				<div class="shopstyle-item__inner">
					<a href="<?php echo $image['product']['clickUrl'];?>" target="_blank">
						<img src="<?php echo $image['product']['image']['sizes']['IPhone']['url'];?>" alt="shopstyle apparel"/>
					</a>
				</div>
			</div>

		   <?php endforeach; ?>

		<?php endif;?>

		<!-- beauty -->

		<?php

			
			if(($shopStyleBeauty = get_transient("shop_style_beauty")) === false) 
			{
				$jsonBeauty = file_get_contents('http://api.shopstyle.com/api/v2/lists/47077716/items?pid=uid6041-27578179-93&limit=20');
				$shopStyleBeauty = json_decode( $jsonBeauty , true );
				set_transient("shop_style_beauty", $shopStyleBeauty, 3600);
			} 
			
		// If we have shopStyleApparel
		if ( false !== $shopStyleBeauty ) : ?>

		   	<?php// print_r($shopStyleBeauty['favorites']);?>


		   <?php foreach( $shopStyleBeauty['favorites'] as $image ):?>

		   	<div class="shopstyle-item shopstyle-item--beauty">
				<div class="shopstyle-item__inner">
					<a href="<?php echo $image['product']['clickUrl'];?>" target="_blank">
						<img src="<?php echo $image['product']['image']['sizes']['IPhone']['url'];?>" alt="shopstyle apparel"/>
					</a>
				</div>
			</div>

		   <?php endforeach; ?>

		<?php endif;?>


		<!-- accessories -->

		<?php

			
			if(($shopStyleAccessories = get_transient("shop_style_accessories")) === false) 
			{
				$jsonAccessories = file_get_contents('http://api.shopstyle.com/api/v2/lists/47077454/items?pid=uid6041-27578179-93&limit=20');
				$shopStyleAccessories = json_decode( $jsonAccessories , true );
				set_transient("shop_style_accessories", $shopStyleAccessories, 3600);
			} 
			
		// If we have shopStyleApparel
		if ( false !== $shopStyleAccessories ) : ?>

		   	<?php// print_r($shopStyleBeauty['favorites']);?>


		   <?php foreach( $shopStyleAccessories['favorites'] as $image ):?>

		   	<div class="shopstyle-item shopstyle-item--accessories">
				<div class="shopstyle-item__inner">
					<a href="<?php echo $image['product']['clickUrl'];?>" target="_blank">
						<img src="<?php echo $image['product']['image']['sizes']['IPhone']['url'];?>" alt="shopstyle apparel"/>
					</a>
				</div>
			</div>

		   <?php endforeach; ?>

		<?php endif;?>


		<!-- child -->

		<?php

			
			if(($shopStyleChild = get_transient("shop_style_child")) === false) 
			{
				$jsonChild = file_get_contents('http://api.shopstyle.com/api/v2/lists/47077625/items?pid=uid6041-27578179-93&limit=20');
				$shopStyleChild = json_decode( $jsonChild , true );
				set_transient("shop_style_child", $shopStyleChild, 3600);
			} 
			
		// If we have shopStyleApparel
		if ( false !== $shopStyleChild ) : ?>

		   	<?php// print_r($shopStyleBeauty['favorites']);?>


		   <?php foreach( $shopStyleChild['favorites'] as $image ):?>

		   	<div class="shopstyle-item shopstyle-item--child">
				<div class="shopstyle-item__inner">
					<a href="<?php echo $image['product']['clickUrl'];?>" target="_blank">
						<img src="<?php echo $image['product']['image']['sizes']['IPhone']['url'];?>" alt="shopstyle apparel"/>
					</a>
				</div>
			</div>

		   <?php endforeach; ?>

		<?php endif;?>

	</div>
</div>
